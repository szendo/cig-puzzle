val discs = setOf(
    setOf(0),
    setOf(0, 1),
    setOf(0, 2),
    setOf(0, 3),
    setOf(0, 1, 2),
    setOf(0, 1, 3),
    setOf(0, 2, 4),
    setOf(0, 1, 2, 3),
    setOf(0, 1, 2, 4),
    setOf(0, 1, 3, 4),
    setOf(0, 1, 2, 3, 4),
    setOf(0, 1, 2, 3, 4, 5),
)

val discRotations = discs.map { disc -> (0..5).map { n -> disc.map { (it + n) % 6 }.toSet() }.toSet() }.toSet()

fun isValid(state: List<Set<Int>>, partial: Boolean = false) = (listOf(emptySet<Int>()) + state).zipWithNext()
    .withIndex().all { (i, v) ->
        (v.second - v.first).all { n ->
            state.drop(i).takeWhile { n in it }.count().let { len -> len == 3 || (partial && i + len == state.size) }
        }
    }

fun solve(state: List<Set<Int>>, remaining: Set<Set<Set<Int>>>): List<Set<Int>>? {
    if (remaining.isEmpty()) {
        return if (isValid(state)) state else null
    }

    if (!isValid(state, partial = true)) return null

    remaining.forEach { discWithRot ->
        discWithRot.forEach { disc ->
            solve(state.plusElement(disc), remaining.minusElement(discWithRot))?.let { return it }
        }
    }

    return null
}

fun main() {
    val solution = solve(emptyList(), discRotations) ?: error("No solution")
    println(solution.joinToString("\n") { disc -> (0..5).joinToString(" ") { if (it in disc) "#" else " " } })
}
